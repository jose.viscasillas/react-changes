import { CSSProperties } from 'react';

export interface IState {
  [key: string]: any;
}

export interface IVersion extends IState {
  _id: string;
}

export interface IGlobalStateContext {
  globalState: IState;
  updateGlobalState(updatedState: IState, isUserChange?: boolean): void;
  getState(path: string): any;
  get: (path: string) => any;
  setState(path: string, value: any): void;
  set: (path: string, value: any) => void;
  undoState(): void;
  undo: () => void;
  redoState(): void;
  redo: () => void;
  clearHistory(): void;
  clear: () => void;
  getVersions(): IVersion[];
  versions: () => IVersion[];
  playHistory(ms?: number): void;
  play: (ms?: number) => void;
  loadVersion(id: string): void;
  load: (id: string) => void;
  seekVersion(id: string): void;
  seek: (id: string) => void;
  stopHistory(): void;
  stop: () => void;
}

export declare const GlobalStateProvider: React.FC<IGlobalStateProviderProps>;

export declare const useGlobalState: () => IGlobalStateContext;

export declare const useEarth: (
  path: string,
  defaultValue?: any
) => [any, (value: any) => void];

export declare const useEarthControls: () => {
  undo: () => void;
  play: () => void;
  stop: () => void;
  clear: () => void;
  versions: () => IVersion[];
  seek: (id: string) => void;
};

export declare const EarthControls: React.FC;

export declare const Earth: typeof GlobalStateProvider;

export declare const styles: {
  container: CSSProperties;
  action: CSSProperties;
};
