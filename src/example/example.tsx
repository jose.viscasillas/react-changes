import {
  useChange,
  // EarthControls,
} from "../lib/index.js";

function Example() {
  // React Earth Stuff
  // we are using the useEarth hook to get the count value
  const [count, setCount] = useChange("count", 0);
  const [count2, setCount2] = useChange(0);
  return (
    <>
      <h1>Example</h1>

      <div>
        <code style={{ fontSize: 18, display: "flex", padding: 10 }}>
          {count}
        </code>
        <button onClick={() => setCount(count + 1)}>Increment Counter</button>
        <button onClick={() => setCount(count - 1)}>Decrement Counter</button>
      </div>
      <div>
        <code style={{ fontSize: 18, display: "flex", padding: 10 }}>
          {count2}
        </code>
        <button onClick={() => setCount2(count2 + 1)}>Increment Counter</button>
        <button onClick={() => setCount2(count2 - 1)}>Decrement Counter</button>
      </div>
    </>
  );
}

export default Example;
