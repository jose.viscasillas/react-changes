import React from 'react'
import ReactDOM from 'react-dom/client'
import Example from './example/example.tsx'
import './index.css'
import { Changes } from './lib/index.tsx'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Changes debug={true} controls={true}>
      <Example />
    </Changes>
  </React.StrictMode>,
)


// export { Earth, useEarth, EarthControls, useEarthControls } from './react-earth/index.tsx'